package com.jet2tt.article.di

import com.jet2tt.article.network.Client
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun getRetrofitInstance(): Client {
       return Retrofit.Builder()
           .addConverterFactory(GsonConverterFactory.create())
           .baseUrl("https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/")
           .build()
           .create(Client::class.java)
    }

}