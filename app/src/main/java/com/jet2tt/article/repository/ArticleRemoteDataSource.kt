package com.jet2tt.article.repository

import com.jet2tt.article.network.Client
import javax.inject.Inject

class ArticleRemoteDataSource @Inject constructor(private val retrofitService:Client) {

    suspend fun getArticles(page:Int) = retrofitService.getArticles(page,10)
}