package com.jet2tt.article.repository

import javax.inject.Inject

class ArticleRepository @Inject constructor(private val remoteDataSource: ArticleRemoteDataSource) {

    suspend fun getArticlesList(page:Int) = remoteDataSource.getArticles(page)

}