package com.jet2tt.article.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}