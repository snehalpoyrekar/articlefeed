package com.jet2tt.article.ui.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jet2tt.article.ArticleApplication
import com.jet2tt.article.R
import com.jet2tt.article.model.ArticleModel
import com.jet2tt.article.ui.adapter.ArticleAdapter
import com.jet2tt.article.ui.viewmodel.MainViewModel
import com.jet2tt.article.utils.AppUtils
import com.jet2tt.article.utils.Status
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainViewModel: MainViewModel
    lateinit var adapter: ArticleAdapter

    private var page: Int = 1
    private var isLoading: Boolean = false
    private var isMoreDataAvailable: Boolean = true
    private var pastVisibleItems: Int = 0
    private var pageSize: Int = 10
    private var totalItemCount: Int = 0
    private var visibleItemCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as ArticleApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpViews()
        if (AppUtils.isConnected(this@MainActivity))
            setUpObservable(page)
        else
            Toast.makeText(
                this@MainActivity,
                getString(R.string.no_internet),
                Toast.LENGTH_SHORT
            ).show()
    }


    private fun setUpViews() {
        val layoutManager = LinearLayoutManager(this)
        adapter = ArticleAdapter(arrayListOf())
        recFeed.layoutManager = layoutManager
        recFeed.setHasFixedSize(true)
        recFeed.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        recFeed.adapter = adapter

        recFeed.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.childCount
                    totalItemCount = layoutManager.itemCount
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                    if (!isLoading) {
                        if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                            isLoading = true
                            page++
                            if (isMoreDataAvailable)
                                if (AppUtils.isConnected(this@MainActivity))
                                    setUpObservable(page)
                                else
                                    Toast.makeText(
                                        this@MainActivity,
                                        getString(R.string.no_internet),
                                        Toast.LENGTH_SHORT
                                    ).show()
                        }
                    }
                }
            }
        })
    }

    private fun setUpObservable(page:Int) {
        mainViewModel.getArticleList(page).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        progressBar.visibility = View.GONE
                        resource.data?.let { articles -> retrieveList(articles)
                            if (articles.size < pageSize)
                                isMoreDataAvailable = false
                        }
                    }
                    Status.ERROR -> {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        progressBar.visibility = View.VISIBLE

                    }
                }
            }
        })
    }

    private fun retrieveList(articles: List<ArticleModel>) {
        adapter.apply {
            addArticles(articles)
        }
    }


}