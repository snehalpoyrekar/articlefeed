package com.jet2tt.article.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jet2tt.article.R
import com.jet2tt.article.model.ArticleModel
import kotlinx.android.synthetic.main.item_layout.view.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.ln
import kotlin.math.pow

class ArticleAdapter(private val articleList: MutableList<ArticleModel>) :
    RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>() {

    class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(article: ArticleModel) {
            itemView.apply {
                lblUserName.text = article.user[0].name.plus(article.user[0].lastname)
                lblDesignation.text = article.user[0].designation
                lblCreatedAt.text = article.createdAt.toDate().formatTo("dd-MMM @ HH:MM")
                lblContent.text = article.content
                when (article.likes) {
                    0 -> {
                        lblLikes.visibility = View.GONE
                    }
                    in 1..999 -> {
                        lblLikes.text = article.likes.toString().plus(" Likes")
                    }
                    else -> {
                        val likes: String = numberFormat(article.likes)
                        lblLikes.text = likes.toString().plus(" Likes")
                    }
                }

                when (article.comments) {
                    0 -> {
                        lblComments.visibility = View.GONE
                    }
                    in 1..999 -> {
                        lblComments.text = article.comments.toString().plus(" Comments")
                    }
                    else -> {
                        val comments: String = numberFormat(article.comments)
                        lblComments.text = comments.toString().plus(" Comments")
                    }
                }

                Glide.with(imgUser.context)
                    .load(article.user[0].avatar).placeholder(R.drawable.ic_launcher_foreground)
                    .into(imgUser)

                if (article.media.isNotEmpty()) {
                    lblTitle.visibility = View.VISIBLE
                    lblUrl.visibility = View.VISIBLE
                    imgArticle.visibility = View.VISIBLE

                    lblTitle.text = article.media[0].title
                    lblUrl.text = article.media[0].url

                    Glide.with(imgArticle.context)
                        .load(article.media[0].image).placeholder(R.drawable.ic_launcher_foreground)
                        .centerCrop()
                        .into(imgArticle)

                } else {
                    lblTitle.visibility = View.GONE
                    lblUrl.visibility = View.GONE
                    imgArticle.visibility = View.GONE
                }
            }
        }


        private fun numberFormat(count: Int): String {
            if (count < 1000) return "" + count
            val exp =
                (ln(count.toDouble()) / ln(1000.0)).toInt()
            val format = DecimalFormat("0.#")
            val value: String =
                format.format(count / 1000.0.pow(exp.toDouble()))
            return String.format("%s%c", value, "kMBTPE"[exp - 1])
        }

        private fun String.toDate(
            dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss",
            timeZone: TimeZone = TimeZone.getTimeZone("UTC")
        ): Date {
            val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
            parser.timeZone = timeZone
            return parser.parse(this)
        }

        fun Date.formatTo(dateFormat: String, timeZone: TimeZone = TimeZone.getDefault()): String {
            val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
            formatter.timeZone = timeZone
            return formatter.format(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder =
        ArticleViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        )

    override fun getItemCount(): Int = articleList.size

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.bind(articleList[position])
    }

    fun addArticles(articles: List<ArticleModel>) {
        this.articleList.apply {
            addAll(articles)
            notifyDataSetChanged()
        }
    }
}