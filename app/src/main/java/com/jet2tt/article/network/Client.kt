package com.jet2tt.article.network

import com.jet2tt.article.model.ArticleModel
import retrofit2.http.GET
import retrofit2.http.Query

interface Client {

    @GET("blogs")
    suspend fun getArticles(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): List<ArticleModel>
}