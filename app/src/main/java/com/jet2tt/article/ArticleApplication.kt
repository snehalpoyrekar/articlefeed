package com.jet2tt.article

import android.app.Application
import com.jet2tt.article.di.AppComponent
import com.jet2tt.article.di.DaggerAppComponent

open class ArticleApplication : Application() {

    val appComponent: AppComponent by lazy {
        initializeComponent()
    }

    open fun initializeComponent(): AppComponent {
        return DaggerAppComponent.factory().create(applicationContext)
    }

}