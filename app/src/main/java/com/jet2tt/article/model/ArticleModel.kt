package com.jet2tt.article.model

data class ArticleModel(
    val comments: Int,
    val content: String,
    val createdAt: String,
    val id: String,
    val likes: Int,
    val media: List<Media>,
    val user: List<User>
)